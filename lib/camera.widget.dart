import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';

class CameraCapture extends StatefulWidget {
  const CameraCapture({super.key});

  @override
  State<CameraCapture> createState() => _CameraCaptureState();
}

class _CameraCaptureState extends State<CameraCapture> {
  late CameraController _controller;

  // TODO : a mettre dans un service
  List<CameraDescription> _cameras = <CameraDescription>[];

  bool _isLoading = true;

  // TODO : a mettre dans un service
  final decoder = QRCodeDartScanDecoder(formats: [
    BarcodeFormat.QR_CODE,
    BarcodeFormat.AZTEC,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.PDF_417,
    BarcodeFormat.CODE_39,
    BarcodeFormat.CODE_93,
    BarcodeFormat.CODE_128,
    BarcodeFormat.EAN_8,
    BarcodeFormat.EAN_13,
  ]);

  @override
  void initState() {
    availableCameras().then((value) {
      _cameras = value;
      _initializeCameraController();
    }).catchError((e) => _logError(e.code, e.description));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(1.0),
            child: Center(
              child: _isLoading ? const CircularProgressIndicator() : CameraPreview(_controller),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _initializeCameraController() async {
    _controller = CameraController(
      _cameras[0],
      ResolutionPreset.medium,
    );

    try {
      await _controller.initialize();

      setState(() {
        _isLoading = false;
      });

      bool processing = false;

      _controller.startImageStream((CameraImage image) {
        if (!processing) {
          processing = true;
          decoder.decodeCameraImage(image).then((Result? value) {
            if (value != null) {
              print('DEBUG -- cameraController.addListener ; found a QRCode with value $value');
            }
            processing = false;
          });

          // TODO: ajouter la gestion de l'OCR
        }
      });
    } on CameraException catch (e) {
      _logError('CameraException', e.toString());
    }
  }
}

void _logError(String code, String? message) {
  print('Error: $code${message == null ? '' : '\nError Message: $message'}');
}
