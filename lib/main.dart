// ignore_for_file: avoid_print
import 'package:camera_poc/camera.widget.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const CameraApp());
}

class CameraApp extends StatelessWidget {
  const CameraApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: _CameraHome(),
    );
  }
}

class _CameraHome extends StatefulWidget {
  const _CameraHome();

  @override
  State<_CameraHome> createState() => _CameraHomeState();
}

class _CameraHomeState extends State<_CameraHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Camera example'),
      ),
      body: const CameraCapture(),
    );
  }
}
